# Contributors

* wilkie
* Luís Oliveira
* jason
* Austin Oldham
* Nate Stump
* Danny Phan
* Patrick Stoyer
* Tyler Lendon
* Joel Valentino

## Documentation

* wilkie

## Artwork

* Maxicons - Dinosaurs (CCBY, licensed)
* wilkie - RAWRS Icon

## TinyEMU

* Fabrice Bellard
* wilkie (modifications)
